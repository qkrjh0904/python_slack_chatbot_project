### Python_Slack_Chatbot_Project

# 레시피 검색 챗봇

SSAFY 구미 3반
PYTHON SLACK CHATBOT PROJECT

TEAM MEMBER : 박정호, 김상형, 김상정



## 1.기능 : 원하는 요리에 대한 레시피를 제공하는 챗봇

## 2.개발목적 : 요리를 잘 못하는 사람들을 위해 레시피를 쉽게 제공하기 위함

## 3.타겟 : 먹고싶은 음식의 레시피가 궁금한 모든 사람

## 4.장점
    4.1. 원하는 음식을 쉽게 검색가능하다.
    4.2. 해먹지수(평가점수)로 음식의 맛을 신뢰할 수 있다.
    4.3. 조리시간을 알 수 있다.
    4.4. 얼마나 많은 사람들이 이용했는지 알 수 있다.

## 5.기능 : 해먹남녀 페이지 크롤링을 바탕으로 이미지, 설명, 링크 제공

## 6.제공내용
    6.1. 검색 음식 이미지
    6.2. 음식 레시피 링크
    6.3. 해먹지수
    6.4. 조리시간
    6.5. 좋아요 수

## 7.역할분담
    박정호 : 음식 정보 크롤링, Git 관리
    김상형 : 이미지 크롤링, 한글 인코딩
    김상정 : 챗봇 초기 시나리오 코드 구현, 발표자료준비

## 8.개발환경 : VScode, Slack, python3.7
